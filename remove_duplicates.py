#!/usr/bin/env python

# Opens txt specified by the user with list of AOD locations and removes all
# lines not containing the substring, chosen by the user.
import sys
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-f", "--file", dest = "filename",
		          help = "input filelist.txt")

parser.add_argument("-s", "--substring", dest = "substring",
				  help = "Substring common to all filenames you wish to keep")

args = parser.parse_args()

if (not args.filename or not args.substring):
	if (not args.filename):
		parser.error("Please specify input AOD filelist! Exiting ...")
	if (not args.substring):
		parser.error("Please specify substring contained in path of desired files! Exiting ...")
	
	sys.exit(1)

input_filename = args.filename
save_substring = args.substring

with open(input_filename, 'r') as f:
	lines = f.readlines()
with open(input_filename, 'w') as f:
	for line in lines:
		if (save_substring in line):
			f.write(line)
