# setup ATLAS
setupATLAS
# Analysis Base 21.2.104
asetup 21.2.104,AnalysisBase
# grid proxy
voms-proxy-init -voms atlas
# setup rucio
lsetup rucio
