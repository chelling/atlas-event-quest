#!/usr/bin/env python

# ---------------------------------- IMPORTS ------------------------------- #

import ROOT
import sys
import argparse
import pandas as pd

# ---------------------------------- FUNCTIONS ------------------------------- #

def write_results(found_runNumbers, found_eventNumbers, found_filenames):
	found_data = {"runNumber"   : found_runNumbers,
			  	  "eventNumber" : found_eventNumbers,
			  	  "files:"      : found_filenames}

	found_df = pd.DataFrame(found_data)
	found_df.to_csv(output_filename, sep=",")

# ------------------------------------ INPUT --------------------------------- #

parser = argparse.ArgumentParser()

parser.add_argument("-t", "--txtfile", dest = "txt_filename",
				    help = "input txt file with AOD file locations")

parser.add_argument("-c", "--csvfile", dest = "csv_filename",
				    help = "input txt file with AOD file locations")

parser.add_argument("-o", "--outfile", dest = "out_filename",
				    help = "output csv file with found run/event numbers and file locations")

parser.add_argument("-n", "--number" , dest = "event_count",
				  help = "Number of events to find before quiting")

args = parser.parse_args()

if (not args.txt_filename or not args.csv_filename):
	if (not args.txt_filename):
		parser.error("Please specify the txt file with AOD file locations! Exiting ...")
	if (not args.csv_filename):
		parser.error("Please specify the csv file with run/event numbers! Exiting ...")
	
	sys.exit(1)

# input files
filelist  = args.txt_filename
eventlist = args.csv_filename

# quit after finding n events (-1 means just keep going)
n_events = -1

if (args.event_count):
	n_events = int(args.event_count)

# default output filename
output_filename = 'Output/output.csv'
if (args.out_filename):
	output_filename = 'Output/' + args.out_filename

csv_data = pd.read_csv(eventlist)
csv_df   = pd.DataFrame(csv_data)

# -------------------------------- END INPUT --------------------------------- #

# Check that we have ability to access xAODs.
if (not ROOT.xAOD.Init().isSuccess()):
    print("Failed xAOD.Init()")
    sys.exit(1)

# Store found values
found_runNumbers   = []
found_eventNumbers = []
found_filenames    = []
# Keep track of events found (if n_events != -1)
found_events       = 0

fh = open(filelist)
for line in fh:
	# get fileName
	filename = line.strip()
	# Skip if blank
	if (filename == ''): continue
	# Root File
	f = ROOT.TFile.Open(filename)
	# Tree
	t = ROOT.xAOD.MakeTransientTree(f, "CollectionTree")
	
	# Let the hunt begin
	for i in range(t.GetEntries()):
		t.GetEntry(i)
		eventNumber = t.EventInfo.eventNumber()
		runNumber = t.EventInfo.runNumber()
		if (any(eventNumber == csv_df.eventNumber) and any(runNumber == csv_df.runNumber)):
			print('VICTORY! ', runNumber, eventNumber, filename)
			# Cast run/event number as int to remove pesky 'L' at end of int
			found_runNumbers.append(int(runNumber))
			found_eventNumbers.append(int(eventNumber))
			found_filenames.append(filename)

			if (n_events != -1):
				found_events += 1
				if (found_events == n_events):
					print("Mischief Managed ... Peace out")
					write_results(found_runNumbers, found_eventNumbers, found_filenames)
					sys.exit(0)
					f.Close()

if (n_events == -1):
	write_results(found_runNumbers, found_eventNumbers, found_filenames)
