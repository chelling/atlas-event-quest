# Atlas Event Quest

# Introduction
This tool allows a user to specify a list of run numbers as well as a list of AODs to search. This is handy for selecting events for an event display since event peaking isn't available for all files.

# Installation
```sh
git clone ssh://git@gitlab.cern.ch:7999/chelling/atlas-event-quest.git
cd atlas-event-quest
source setup.sh
```
## Every Login:
```
cd atlas-event-quest
source setup.sh
```

# Make Filelist
Make a list of files from your dataset ID:
```
rucio list-file replicas --pfns --protocols root --sort geoip [rucio-did] >& filelist.txt &
```

## Example
```
rucio list-file-replicas --pfns --protocols root --sort geoip data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EXOT8.grp17_v01_p3730 >& FileLists/AOD_filelist.txt &
```

To view the list as it's being made:
```
tail -f FileLists/AOD_filelist.txt
```
---
**NOTE**

This can take a while with large dids

---

# Remove Filelist Duplicates
The rucio list will contain several duplicates. Use the **remove_duplicates.py** script to remove unwanted lines from the filelist.  Since one copy from a particular site is enough, just pick a substring unique to the site you would like to use.

```
python remove_duplicates.py -f inputfile.txt -s substring
```

## Example
```
python remove_duplicates.py -f FileLists/AOD_filelist.txt -s usatlas.bnl.gov
```

# Make Run/Event Number List
Format the run and event numbers as a csv

```
runNumber,eventNumber
333367,921922421
333367,1194346226
333367,1157878911
```

An example csv has been included in the **CSVs** directory as **csv_example.csv** and the example AODs corresponding to these events are located in the **Filelists** Directory in **AOD_filelist_example.txt**

---
**NOTE**

I haven't included a script to generate the csv, you'll need to fill this up yourself.

---

# Find Events

Finding events occurs in the **event_quest.py** script.  You must specify the input list of files, as well as the csv containing the desired event/run numbers.

```
python event_quest.py -t inputfile.txt -c run_event_numbers.csv
```

You may also choose the output filename using the _-o outfile.csv_ option, which will be placed in the **Output** Directory (You do _not_ need to speficy the directory).  A default outfile is stored there in the case _-o_ is not used, but it will be overwritten each time the program is run.  Additionally, if you want to stop the search after N events have been found, you can use _-n N_.

## Example

```
python event_quest.py -t FileLists/AOD_filelist_example.txt -c CSVs/csv_example.csv -o example_output.csv -n 2
```

---
**NOTE**

This will all take a very long time to run, so grab a cup of coffee, lunch, a few movies, and I'll see you in a day or two

---

If you want to speed things up, it's possible to split the filelist into several filelists.  Then you just need to run the program multiple times from several terminals, making sure to change input filenames and output filenames.  Functionality for this may be instituted at a later time.
